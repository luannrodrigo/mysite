const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');


const app = express();

//setando a engine de views
app.set('view engine', 'ejs');
//setando o diretório de views
app.set('views', './app/views');

//setando o diretórios onde ficam os utilitarios para o site
app.use(express.static('./app/public'));

//setando o bodyParser para enviar informações dos formularios
app.use(bodyParser.urlencoded({extended: true}));
//setando o expressValidator para validar as insformaçoes fornecidas pelo usuario
app.use(expressValidator())

//setando o diretorios das rotas com o consign
consign()
  .include('app/routes')
  .then('config/dbConnection.js')
  .then('app/models')
  .then('app/controllers')
  .into(app)

module.exports = app;
